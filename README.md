## Krypton Go

## Building the source

Building gkr requires two external dependencies, Go and GMP.
You can install them using your favourite package manager.
Once the dependencies are installed, run

    chmod +x build/*
    make gkr

## Executables

Go Krypton comes with several wrappers/executables found in 
the `cmd` directory

 Command  |         |
----------|---------|
`gkr` | Krypton CLI (krypton command line interface client) |
`bootnode` | runs a bootstrap node for the Discovery Protocol |
`krtest` | test tool which runs with the tests suite: `/path/to/test.json > krtest --test BlockTests --stdin`.
`evm` | is a generic Krypton Virtual Machine: `evm -code 60ff60ff -gas 10000 -price 0 -dump`. See `-h` for a detailed description. |
`disasm` | disassembles EVM code: `echo "6001" | disasm` |
`rlpdump` | prints RLP structures |

## Command line options

`gkr` can be configured via command line options, environment variables and config files.

To get the options available:

    gkr help


## Contribution

If you'd like to contribute to go-krypton please fork, fix, commit and
send a pull request.