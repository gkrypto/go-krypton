// Copyright 2015 The go-krypton Authors
// This file is part of the go-krypton library.
//
// The go-krypton library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-krypton library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-krypton library. If not, see <http://www.gnu.org/licenses/>.

package kr

import (
	"github.com/krypton/go-krypton/metrics"
)

var (
	propTxnInPacketsMeter    = metrics.NewMeter("kr/prop/txns/in/packets")
	propTxnInTrafficMeter    = metrics.NewMeter("kr/prop/txns/in/traffic")
	propTxnOutPacketsMeter   = metrics.NewMeter("kr/prop/txns/out/packets")
	propTxnOutTrafficMeter   = metrics.NewMeter("kr/prop/txns/out/traffic")
	propHashInPacketsMeter   = metrics.NewMeter("kr/prop/hashes/in/packets")
	propHashInTrafficMeter   = metrics.NewMeter("kr/prop/hashes/in/traffic")
	propHashOutPacketsMeter  = metrics.NewMeter("kr/prop/hashes/out/packets")
	propHashOutTrafficMeter  = metrics.NewMeter("kr/prop/hashes/out/traffic")
	propBlockInPacketsMeter  = metrics.NewMeter("kr/prop/blocks/in/packets")
	propBlockInTrafficMeter  = metrics.NewMeter("kr/prop/blocks/in/traffic")
	propBlockOutPacketsMeter = metrics.NewMeter("kr/prop/blocks/out/packets")
	propBlockOutTrafficMeter = metrics.NewMeter("kr/prop/blocks/out/traffic")
	reqHashInPacketsMeter    = metrics.NewMeter("kr/req/hashes/in/packets")
	reqHashInTrafficMeter    = metrics.NewMeter("kr/req/hashes/in/traffic")
	reqHashOutPacketsMeter   = metrics.NewMeter("kr/req/hashes/out/packets")
	reqHashOutTrafficMeter   = metrics.NewMeter("kr/req/hashes/out/traffic")
	reqBlockInPacketsMeter   = metrics.NewMeter("kr/req/blocks/in/packets")
	reqBlockInTrafficMeter   = metrics.NewMeter("kr/req/blocks/in/traffic")
	reqBlockOutPacketsMeter  = metrics.NewMeter("kr/req/blocks/out/packets")
	reqBlockOutTrafficMeter  = metrics.NewMeter("kr/req/blocks/out/traffic")
)
