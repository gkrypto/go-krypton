// Copyright 2014 The go-krypton Authors
// This file is part of the go-krypton library.
//
// The go-krypton library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-krypton library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-krypton library. If not, see <http://www.gnu.org/licenses/>.

// Package kr implements the Krypton protocol.
package kr

import (
	"crypto/ecdsa"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/big"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/krypton/krhash"
	"github.com/krypton/go-krypton/accounts"
	"github.com/krypton/go-krypton/common"
	"github.com/krypton/go-krypton/common/compiler"
	"github.com/krypton/go-krypton/core"
	"github.com/krypton/go-krypton/core/types"
	"github.com/krypton/go-krypton/core/vm"
	"github.com/krypton/go-krypton/crypto"
	"github.com/krypton/go-krypton/kr/downloader"
	"github.com/krypton/go-krypton/krdb"
	"github.com/krypton/go-krypton/event"
	"github.com/krypton/go-krypton/logger"
	"github.com/krypton/go-krypton/logger/glog"
	"github.com/krypton/go-krypton/miner"
	"github.com/krypton/go-krypton/p2p"
	"github.com/krypton/go-krypton/p2p/discover"
	"github.com/krypton/go-krypton/p2p/nat"
	"github.com/krypton/go-krypton/whisper"
)

const (
	epochLength    = 30000
	krhashRevision = 23

	autoDAGcheckInterval = 10 * time.Hour
	autoDAGepochHeight   = epochLength / 2
)

var (
	jsonlogger = logger.NewJsonLogger()

	defaultBootNodes = []*discover.Node{
		// KR/DEV Go Bootnodes
		discover.MustParseNode("enode://0fb920467bdc123eeee3c132519457967227ed80d255fc210b09bc070fb4612df90532604629f91c3f1912c298d5a20ef4c22824d1be33fef270b07448760362@192.99.32.187:17171"),
		discover.MustParseNode("enode://382b74a663581fe3559b52521415f9e7ee596f4873e0e88f8406e0e025d6ed1f2bed55f1f9739cab7d1e247cd5bcc4d944add0c282ea7232155da74b895f0e75@167.114.13.142:17172"),
		discover.MustParseNode("enode://809e1781a9da785ee6084ff38011f771c61ba9dce1e372ca8cbf2de9af119d9df5ded7728bc03f0b765b8ec9d14a3b296e6b248239d6cc8e4b2fbc3d3f9796a1@167.114.13.143:17173"),
	}

	staticNodes  = "static-nodes.json"  // Path within <datadir> to search for the static node list
	trustedNodes = "trusted-nodes.json" // Path within <datadir> to search for the trusted node list
)

type Config struct {
	Name         string
	NetworkId    int
	GenesisNonce int
	GenesisFile  string
	GenesisBlock *types.Block // used by block tests
	Olympic      bool

	BlockChainVersion  int
	SkipBcVersionCheck bool // e.g. blockchain export
	DatabaseCache      int

	DataDir   string
	LogFile   string
	Verbosity int
	LogJSON   string
	VmDebug   bool
	NatSpec   bool
	AutoDAG   bool
	PowTest   bool
	ExtraData []byte

	MaxPeers        int
	MaxPendingPeers int
	Discovery       bool
	Port            string

	// Space-separated list of discovery node URLs
	BootNodes string

	// This key is used to identify the node on the network.
	// If nil, an ephemeral key is used.
	NodeKey *ecdsa.PrivateKey

	NAT  nat.Interface
	Shh  bool
	Dial bool

	Krerbase      common.Address
	GasPrice       *big.Int
	MinerThreads   int
	AccountManager *accounts.Manager
	SolcPath       string

	GpoMinGasPrice          *big.Int
	GpoMaxGasPrice          *big.Int
	GpoFullBlockRatio       int
	GpobaseStepDown         int
	GpobaseStepUp           int
	GpobaseCorrectionFactor int

	// NewDB is used to create databases.
	// If nil, the default is to create leveldb databases on disk.
	NewDB func(path string) (common.Database, error)
}

func (cfg *Config) parseBootNodes() []*discover.Node {
	if cfg.BootNodes == "" {
		return defaultBootNodes
	}
	var ns []*discover.Node
	for _, url := range strings.Split(cfg.BootNodes, " ") {
		if url == "" {
			continue
		}
		n, err := discover.ParseNode(url)
		if err != nil {
			glog.V(logger.Error).Infof("Bootstrap URL %s: %v\n", url, err)
			continue
		}
		ns = append(ns, n)
	}
	return ns
}

// parseNodes parses a list of discovery node URLs loaded from a .json file.
func (cfg *Config) parseNodes(file string) []*discover.Node {
	// Short circuit if no node config is present
	path := filepath.Join(cfg.DataDir, file)
	if _, err := os.Stat(path); err != nil {
		return nil
	}
	// Load the nodes from the config file
	blob, err := ioutil.ReadFile(path)
	if err != nil {
		glog.V(logger.Error).Infof("Failed to access nodes: %v", err)
		return nil
	}
	nodelist := []string{}
	if err := json.Unmarshal(blob, &nodelist); err != nil {
		glog.V(logger.Error).Infof("Failed to load nodes: %v", err)
		return nil
	}
	// Interpret the list as a discovery node array
	var nodes []*discover.Node
	for _, url := range nodelist {
		if url == "" {
			continue
		}
		node, err := discover.ParseNode(url)
		if err != nil {
			glog.V(logger.Error).Infof("Node URL %s: %v\n", url, err)
			continue
		}
		nodes = append(nodes, node)
	}
	return nodes
}

func (cfg *Config) nodeKey() (*ecdsa.PrivateKey, error) {
	// use explicit key from command line args if set
	if cfg.NodeKey != nil {
		return cfg.NodeKey, nil
	}
	// use persistent key if present
	keyfile := filepath.Join(cfg.DataDir, "nodekey")
	key, err := crypto.LoadECDSA(keyfile)
	if err == nil {
		return key, nil
	}
	// no persistent key, generate and store a new one
	if key, err = crypto.GenerateKey(); err != nil {
		return nil, fmt.Errorf("could not generate server key: %v", err)
	}
	if err := crypto.SaveECDSA(keyfile, key); err != nil {
		glog.V(logger.Error).Infoln("could not persist nodekey: ", err)
	}
	return key, nil
}

type Krypton struct {
	// Channel for shutting down the krypton
	shutdownChan chan bool

	// DB interfaces
	chainDb common.Database // Block chain databe
	dappDb  common.Database // Dapp database

	// Closed when databases are flushed and closed
	databasesClosed chan bool

	//*** SERVICES ***
	// State manager for processing new blocks and managing the over all states
	blockProcessor  *core.BlockProcessor
	txPool          *core.TxPool
	chainManager    *core.ChainManager
	accountManager  *accounts.Manager
	whisper         *whisper.Whisper
	pow             *krhash.Krash
	protocolManager *ProtocolManager
	SolcPath        string
	solc            *compiler.Solidity

	GpoMinGasPrice          *big.Int
	GpoMaxGasPrice          *big.Int
	GpoFullBlockRatio       int
	GpobaseStepDown         int
	GpobaseStepUp           int
	GpobaseCorrectionFactor int

	net      *p2p.Server
	eventMux *event.TypeMux
	miner    *miner.Miner

	// logger logger.LogSystem

	Mining        bool
	MinerThreads  int
	NatSpec       bool
	DataDir       string
	AutoDAG       bool
	PowTest       bool
	autodagquit   chan bool
	kryptonbase     common.Address
	clientVersion string
	netVersionId  int
	shhVersionId  int
}

func New(config *Config) (*Krypton, error) {
	// Bootstrap database
	logger.New(config.DataDir, config.LogFile, config.Verbosity)
	if len(config.LogJSON) > 0 {
		logger.NewJSONsystem(config.DataDir, config.LogJSON)
	}

	// Let the database take 3/4 of the max open files (TODO figure out a way to get the actual limit of the open files)
	const dbCount = 3
	krdb.OpenFileLimit = 128 / (dbCount + 1)

	newdb := config.NewDB
	if newdb == nil {
		newdb = func(path string) (common.Database, error) { return krdb.NewLDBDatabase(path, config.DatabaseCache) }
	}

	// attempt to merge database togkrypton, upgrading from an old version
	if err := mergeDatabases(config.DataDir, newdb); err != nil {
		return nil, err
	}

	chainDb, err := newdb(filepath.Join(config.DataDir, "chaindata"))
	if err != nil {
		return nil, fmt.Errorf("blockchain db err: %v", err)
	}
	if db, ok := chainDb.(*krdb.LDBDatabase); ok {
		db.Meter("kr/db/chaindata/")
	}
	dappDb, err := newdb(filepath.Join(config.DataDir, "dapp"))
	if err != nil {
		return nil, fmt.Errorf("dapp db err: %v", err)
	}
	if db, ok := dappDb.(*krdb.LDBDatabase); ok {
		db.Meter("kr/db/dapp/")
	}

	nodeDb := filepath.Join(config.DataDir, "nodes")
	glog.V(logger.Info).Infof("Protocol Versions: %v, Network Id: %v", ProtocolVersions, config.NetworkId)

	if len(config.GenesisFile) > 0 {
		fr, err := os.Open(config.GenesisFile)
		if err != nil {
			return nil, err
		}

		block, err := core.WriteGenesisBlock(chainDb, fr)
		if err != nil {
			return nil, err
		}
		glog.V(logger.Info).Infof("Successfully wrote genesis block. New genesis hash = %x\n", block.Hash())
	}

	if config.Olympic {
		_, err := core.WriteTestNetGenesisBlock(chainDb, 42)
		if err != nil {
			return nil, err
		}
		glog.V(logger.Error).Infoln("Starting Olympic network")
	}

	// This is for testing only.
	if config.GenesisBlock != nil {
		core.WriteBlock(chainDb, config.GenesisBlock)
		core.WriteHead(chainDb, config.GenesisBlock)
	}

	if !config.SkipBcVersionCheck {
		b, _ := chainDb.Get([]byte("BlockchainVersion"))
		bcVersion := int(common.NewValue(b).Uint())
		if bcVersion != config.BlockChainVersion && bcVersion != 0 {
			return nil, fmt.Errorf("Blockchain DB version mismatch (%d / %d). Run gkr upgradedb.\n", bcVersion, config.BlockChainVersion)
		}
		saveBlockchainVersion(chainDb, config.BlockChainVersion)
	}
	glog.V(logger.Info).Infof("Blockchain DB Version: %d", config.BlockChainVersion)

	kr := &Krypton{
		shutdownChan:            make(chan bool),
		databasesClosed:         make(chan bool),
		chainDb:                 chainDb,
		dappDb:                  dappDb,
		eventMux:                &event.TypeMux{},
		accountManager:          config.AccountManager,
		DataDir:                 config.DataDir,
		kryptonbase:               config.Krerbase,
		clientVersion:           config.Name, // TODO should separate from Name
		netVersionId:            config.NetworkId,
		NatSpec:                 config.NatSpec,
		MinerThreads:            config.MinerThreads,
		SolcPath:                config.SolcPath,
		AutoDAG:                 config.AutoDAG,
		PowTest:                 config.PowTest,
		GpoMinGasPrice:          config.GpoMinGasPrice,
		GpoMaxGasPrice:          config.GpoMaxGasPrice,
		GpoFullBlockRatio:       config.GpoFullBlockRatio,
		GpobaseStepDown:         config.GpobaseStepDown,
		GpobaseStepUp:           config.GpobaseStepUp,
		GpobaseCorrectionFactor: config.GpobaseCorrectionFactor,
	}

	if config.PowTest {
		glog.V(logger.Info).Infof("krhash used in test mode")
		kr.pow, err = krhash.NewForTesting()
		if err != nil {
			return nil, err
		}
	} else {
		kr.pow = krhash.New()
	}
	//genesis := core.GenesisBlock(uint64(config.GenesisNonce), stateDb)
	kr.chainManager, err = core.NewChainManager(chainDb, kr.pow, kr.EventMux())
	if err != nil {
		if err == core.ErrNoGenesis {
			return nil, fmt.Errorf(`Genesis block not found. Please supply a genesis block with the "--genesis /path/to/file" argument`)
		}

		return nil, err
	}
	kr.txPool = core.NewTxPool(kr.EventMux(), kr.chainManager.State, kr.chainManager.GasLimit)

	kr.blockProcessor = core.NewBlockProcessor(chainDb, kr.pow, kr.chainManager, kr.EventMux())
	kr.chainManager.SetProcessor(kr.blockProcessor)
	kr.protocolManager = NewProtocolManager(config.NetworkId, kr.eventMux, kr.txPool, kr.pow, kr.chainManager)

	kr.miner = miner.New(kr, kr.EventMux(), kr.pow)
	kr.miner.SetGasPrice(config.GasPrice)
	kr.miner.SetExtra(config.ExtraData)

	if config.Shh {
		kr.whisper = whisper.New()
		kr.shhVersionId = int(kr.whisper.Version())
	}

	netprv, err := config.nodeKey()
	if err != nil {
		return nil, err
	}
	protocols := append([]p2p.Protocol{}, kr.protocolManager.SubProtocols...)
	if config.Shh {
		protocols = append(protocols, kr.whisper.Protocol())
	}
	kr.net = &p2p.Server{
		PrivateKey:      netprv,
		Name:            config.Name,
		MaxPeers:        config.MaxPeers,
		MaxPendingPeers: config.MaxPendingPeers,
		Discovery:       config.Discovery,
		Protocols:       protocols,
		NAT:             config.NAT,
		NoDial:          !config.Dial,
		BootstrapNodes:  config.parseBootNodes(),
		StaticNodes:     config.parseNodes(staticNodes),
		TrustedNodes:    config.parseNodes(trustedNodes),
		NodeDatabase:    nodeDb,
	}
	if len(config.Port) > 0 {
		kr.net.ListenAddr = ":" + config.Port
	}

	vm.Debug = config.VmDebug

	return kr, nil
}

type NodeInfo struct {
	Name       string
	NodeUrl    string
	NodeID     string
	IP         string
	DiscPort   int // UDP listening port for discovery protocol
	TCPPort    int // TCP listening port for RLPx
	Td         string
	ListenAddr string
}

func (s *Krypton) NodeInfo() *NodeInfo {
	node := s.net.Self()

	return &NodeInfo{
		Name:       s.Name(),
		NodeUrl:    node.String(),
		NodeID:     node.ID.String(),
		IP:         node.IP.String(),
		DiscPort:   int(node.UDP),
		TCPPort:    int(node.TCP),
		ListenAddr: s.net.ListenAddr,
		Td:         s.ChainManager().Td().String(),
	}
}

type PeerInfo struct {
	ID            string
	Name          string
	Caps          string
	RemoteAddress string
	LocalAddress  string
}

func newPeerInfo(peer *p2p.Peer) *PeerInfo {
	var caps []string
	for _, cap := range peer.Caps() {
		caps = append(caps, cap.String())
	}
	return &PeerInfo{
		ID:            peer.ID().String(),
		Name:          peer.Name(),
		Caps:          strings.Join(caps, ", "),
		RemoteAddress: peer.RemoteAddr().String(),
		LocalAddress:  peer.LocalAddr().String(),
	}
}

// PeersInfo returns an array of PeerInfo objects describing connected peers
func (s *Krypton) PeersInfo() (peersinfo []*PeerInfo) {
	for _, peer := range s.net.Peers() {
		if peer != nil {
			peersinfo = append(peersinfo, newPeerInfo(peer))
		}
	}
	return
}

func (s *Krypton) ResetWithGenesisBlock(gb *types.Block) {
	s.chainManager.ResetWithGenesisBlock(gb)
}

func (s *Krypton) StartMining(threads int) error {
	eb, err := s.Krerbase()
	if err != nil {
		err = fmt.Errorf("Cannot start mining without kryptonbase address: %v", err)
		glog.V(logger.Error).Infoln(err)
		return err
	}

	go s.miner.Start(eb, threads)
	return nil
}

func (s *Krypton) Krerbase() (eb common.Address, err error) {
	eb = s.kryptonbase
	if (eb == common.Address{}) {
		addr, e := s.AccountManager().AddressByIndex(0)
		if e != nil {
			err = fmt.Errorf("kryptonbase address must be explicitly specified")
		}
		eb = common.HexToAddress(addr)
	}
	return
}

// set in js console via admin interface or wrapper from cli flags
func (self *Krypton) SetKrerbase(kryptonbase common.Address) {
	self.kryptonbase = kryptonbase
	self.miner.SetKrerbase(kryptonbase)
}

func (s *Krypton) StopMining()         { s.miner.Stop() }
func (s *Krypton) IsMining() bool      { return s.miner.Mining() }
func (s *Krypton) Miner() *miner.Miner { return s.miner }

// func (s *Krypton) Logger() logger.LogSystem             { return s.logger }
func (s *Krypton) Name() string                         { return s.net.Name }
func (s *Krypton) AccountManager() *accounts.Manager    { return s.accountManager }
func (s *Krypton) ChainManager() *core.ChainManager     { return s.chainManager }
func (s *Krypton) BlockProcessor() *core.BlockProcessor { return s.blockProcessor }
func (s *Krypton) TxPool() *core.TxPool                 { return s.txPool }
func (s *Krypton) Whisper() *whisper.Whisper            { return s.whisper }
func (s *Krypton) EventMux() *event.TypeMux             { return s.eventMux }
func (s *Krypton) ChainDb() common.Database             { return s.chainDb }
func (s *Krypton) DappDb() common.Database              { return s.dappDb }
func (s *Krypton) IsListening() bool                    { return true } // Always listening
func (s *Krypton) PeerCount() int                       { return s.net.PeerCount() }
func (s *Krypton) Peers() []*p2p.Peer                   { return s.net.Peers() }
func (s *Krypton) MaxPeers() int                        { return s.net.MaxPeers }
func (s *Krypton) ClientVersion() string                { return s.clientVersion }
func (s *Krypton) KrVersion() int                      { return int(s.protocolManager.SubProtocols[0].Version) }
func (s *Krypton) NetVersion() int                      { return s.netVersionId }
func (s *Krypton) ShhVersion() int                      { return s.shhVersionId }
func (s *Krypton) Downloader() *downloader.Downloader   { return s.protocolManager.downloader }

// Start the krypton
func (s *Krypton) Start() error {
	jsonlogger.LogJson(&logger.LogStarting{
		ClientString:    s.net.Name,
		ProtocolVersion: s.KrVersion(),
	})
	err := s.net.Start()
	if err != nil {
		return err
	}
	// periodically flush databases
	go s.syncDatabases()

	if s.AutoDAG {
		s.StartAutoDAG()
	}

	s.protocolManager.Start()

	if s.whisper != nil {
		s.whisper.Start()
	}

	glog.V(logger.Info).Infoln("Server started")
	return nil
}

// sync databases every minute. If flushing fails we exit immediatly. The system
// may not continue under any circumstances.
func (s *Krypton) syncDatabases() {
	ticker := time.NewTicker(1 * time.Minute)
done:
	for {
		select {
		case <-ticker.C:
			// don't change the order of database flushes
			if err := s.dappDb.Flush(); err != nil {
				glog.Fatalf("fatal error: flush dappDb: %v (Restart your node. We are aware of this issue)\n", err)
			}
			if err := s.chainDb.Flush(); err != nil {
				glog.Fatalf("fatal error: flush chainDb: %v (Restart your node. We are aware of this issue)\n", err)
			}
		case <-s.shutdownChan:
			break done
		}
	}

	s.chainDb.Close()
	s.dappDb.Close()

	close(s.databasesClosed)
}

func (s *Krypton) StartForTest() {
	jsonlogger.LogJson(&logger.LogStarting{
		ClientString:    s.net.Name,
		ProtocolVersion: s.KrVersion(),
	})
}

// AddPeer connects to the given node and maintains the connection until the
// server is shut down. If the connection fails for any reason, the server will
// attempt to reconnect the peer.
func (self *Krypton) AddPeer(nodeURL string) error {
	n, err := discover.ParseNode(nodeURL)
	if err != nil {
		return fmt.Errorf("invalid node URL: %v", err)
	}
	self.net.AddPeer(n)
	return nil
}

func (s *Krypton) Stop() {
	s.net.Stop()
	s.chainManager.Stop()
	s.protocolManager.Stop()
	s.txPool.Stop()
	s.eventMux.Stop()
	if s.whisper != nil {
		s.whisper.Stop()
	}
	s.StopAutoDAG()

	close(s.shutdownChan)
}

// This function will wait for a shutdown and resumes main thread execution
func (s *Krypton) WaitForShutdown() {
	<-s.databasesClosed
	<-s.shutdownChan
}

// StartAutoDAG() spawns a go routine that checks the DAG every autoDAGcheckInterval
// by default that is 10 times per epoch
// in epoch n, if we past autoDAGepochHeight within-epoch blocks,
// it calls krhash.MakeDAG  to pregenerate the DAG for the next epoch n+1
// if it does not exist yet as well as remove the DAG for epoch n-1
// the loop quits if autodagquit channel is closed, it can safely restart and
// stop any number of times.
// For any more sophisticated pattern of DAG generation, use CLI subcommand
// makedag
func (self *Krypton) StartAutoDAG() {
	if self.autodagquit != nil {
		return // already started
	}
	go func() {
		glog.V(logger.Info).Infof("Automatic pregeneration of krhash DAG ON (krhash dir: %s)", krhash.DefaultDir)
		var nextEpoch uint64
		timer := time.After(0)
		self.autodagquit = make(chan bool)
		for {
			select {
			case <-timer:
				glog.V(logger.Info).Infof("checking DAG (krhash dir: %s)", krhash.DefaultDir)
				currentBlock := self.ChainManager().CurrentBlock().NumberU64()
				thisEpoch := currentBlock / epochLength
				if nextEpoch <= thisEpoch {
					if currentBlock%epochLength > autoDAGepochHeight {
						if thisEpoch > 0 {
							previousDag, previousDagFull := dagFiles(thisEpoch - 1)
							os.Remove(filepath.Join(krhash.DefaultDir, previousDag))
							os.Remove(filepath.Join(krhash.DefaultDir, previousDagFull))
							glog.V(logger.Info).Infof("removed DAG for epoch %d (%s)", thisEpoch-1, previousDag)
						}
						nextEpoch = thisEpoch + 1
						dag, _ := dagFiles(nextEpoch)
						if _, err := os.Stat(dag); os.IsNotExist(err) {
							glog.V(logger.Info).Infof("Pregenerating DAG for epoch %d (%s)", nextEpoch, dag)
							err := krhash.MakeDAG(nextEpoch*epochLength, "") // "" -> krhash.DefaultDir
							if err != nil {
								glog.V(logger.Error).Infof("Error generating DAG for epoch %d (%s)", nextEpoch, dag)
								return
							}
						} else {
							glog.V(logger.Error).Infof("DAG for epoch %d (%s)", nextEpoch, dag)
						}
					}
				}
				timer = time.After(autoDAGcheckInterval)
			case <-self.autodagquit:
				return
			}
		}
	}()
}

// stopAutoDAG stops automatic DAG pregeneration by quitting the loop
func (self *Krypton) StopAutoDAG() {
	if self.autodagquit != nil {
		close(self.autodagquit)
		self.autodagquit = nil
	}
	glog.V(logger.Info).Infof("Automatic pregeneration of krhash DAG OFF (krhash dir: %s)", krhash.DefaultDir)
}

func (self *Krypton) Solc() (*compiler.Solidity, error) {
	var err error
	if self.solc == nil {
		self.solc, err = compiler.New(self.SolcPath)
	}
	return self.solc, err
}

// set in js console via admin interface or wrapper from cli flags
func (self *Krypton) SetSolc(solcPath string) (*compiler.Solidity, error) {
	self.SolcPath = solcPath
	self.solc = nil
	return self.Solc()
}

// dagFiles(epoch) returns the two alternative DAG filenames (not a path)
// 1) <revision>-<hex(seedhash[8])> 2) full-R<revision>-<hex(seedhash[8])>
func dagFiles(epoch uint64) (string, string) {
	seedHash, _ := krhash.GetSeedHash(epoch * epochLength)
	dag := fmt.Sprintf("full-R%d-%x", krhashRevision, seedHash[:8])
	return dag, "full-R" + dag
}

func saveBlockchainVersion(db common.Database, bcVersion int) {
	d, _ := db.Get([]byte("BlockchainVersion"))
	blockchainVersion := common.NewValue(d).Uint()

	if blockchainVersion == 0 {
		db.Put([]byte("BlockchainVersion"), common.NewValue(bcVersion).Bytes())
	}
}

// mergeDatabases when required merge old database layout to one single database
func mergeDatabases(datadir string, newdb func(path string) (common.Database, error)) error {
	// Check if already upgraded
	data := filepath.Join(datadir, "chaindata")
	if _, err := os.Stat(data); !os.IsNotExist(err) {
		return nil
	}
	// make sure it's not just a clean path
	chainPath := filepath.Join(datadir, "blockchain")
	if _, err := os.Stat(chainPath); os.IsNotExist(err) {
		return nil
	}
	glog.Infoln("Database upgrade required. Upgrading...")

	database, err := newdb(data)
	if err != nil {
		return fmt.Errorf("creating data db err: %v", err)
	}
	defer database.Close()

	// Migrate blocks
	chainDb, err := newdb(chainPath)
	if err != nil {
		return fmt.Errorf("state db err: %v", err)
	}
	defer chainDb.Close()

	if chain, ok := chainDb.(*krdb.LDBDatabase); ok {
		glog.Infoln("Merging blockchain database...")
		it := chain.NewIterator()
		for it.Next() {
			database.Put(it.Key(), it.Value())
		}
		it.Release()
	}

	// Migrate state
	stateDb, err := newdb(filepath.Join(datadir, "state"))
	if err != nil {
		return fmt.Errorf("state db err: %v", err)
	}
	defer stateDb.Close()

	if state, ok := stateDb.(*krdb.LDBDatabase); ok {
		glog.Infoln("Merging state database...")
		it := state.NewIterator()
		for it.Next() {
			database.Put(it.Key(), it.Value())
		}
		it.Release()
	}

	// Migrate transaction / receipts
	extraDb, err := newdb(filepath.Join(datadir, "extra"))
	if err != nil {
		return fmt.Errorf("state db err: %v", err)
	}
	defer extraDb.Close()

	if extra, ok := extraDb.(*krdb.LDBDatabase); ok {
		glog.Infoln("Merging transaction database...")

		it := extra.NewIterator()
		for it.Next() {
			database.Put(it.Key(), it.Value())
		}
		it.Release()
	}

	return nil
}
