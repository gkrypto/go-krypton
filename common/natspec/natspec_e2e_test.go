// Copyright 2015 The go-krypton Authors
// This file is part of the go-krypton library.
//
// The go-krypton library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-krypton library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-krypton library. If not, see <http://www.gnu.org/licenses/>.

package natspec

import (
	"fmt"
	"io/ioutil"
	"math/big"
	"os"
	"runtime"
	"strings"
	"testing"
	"time"

	"github.com/krypton/go-krypton/accounts"
	"github.com/krypton/go-krypton/common"
	"github.com/krypton/go-krypton/common/docserver"
	"github.com/krypton/go-krypton/common/registrar"
	"github.com/krypton/go-krypton/core"
	"github.com/krypton/go-krypton/crypto"
	"github.com/krypton/go-krypton/kr"
	"github.com/krypton/go-krypton/krdb"
	xe "github.com/krypton/go-krypton/xkr"
)

const (
	testBalance = "10000000000000000000"

	testFileName = "long_file_name_for_testing_registration_of_URLs_longer_than_32_bytes.content"

	testNotice = "Register key `utils.toHex(_key)` <- content `utils.toHex(_content)`"

	testExpNotice = "Register key 0xadd1a7d961cff0242089674ec2ef6fca671ab15e1fe80e38859fc815b98d88ab <- content 0xb3a2dea218de5d8bbe6c4645aadbf67b5ab00ecb1a9ec95dbdad6a0eed3e41a7"

	testExpNotice2 = `About to submit transaction (NatSpec notice error: abi key does not match any method): {"params":[{"to":"%s","data": "0x31e12c20"}]}`

	testExpNotice3 = `About to submit transaction (no NatSpec info found for contract: content hash not found for '0x1392c62d05b2d149e22a339c531157ae06b44d39a674cce500064b12b9aeb019'): {"params":[{"to":"%s","data": "0x300a3bbfb3a2dea218de5d8bbe6c4645aadbf67b5ab00ecb1a9ec95dbdad6a0eed3e41a7000000000000000000000000000000000000000000000000000000000000000000000000000000000000000066696c653a2f2f2f746573742e636f6e74656e74"}]}`
)

const (
	testUserDoc = `
{
  "methods": {
    "register(uint256,uint256)": {
      "notice":  "` + testNotice + `"
    }
  },
  "invariants": [
    { "notice": "" }
  ],
  "construction": [
    { "notice": "" }
  ]
}
`
	testAbiDefinition = `
[{
  "name": "register",
  "constant": false,
  "type": "function",
  "inputs": [{
    "name": "_key",
    "type": "uint256"
  }, {
    "name": "_content",
    "type": "uint256"
  }],
  "outputs": []
}]
`

	testContractInfo = `
{
	"userDoc": ` + testUserDoc + `,
	"abiDefinition": ` + testAbiDefinition + `
}
`
)

type testFrontend struct {
	t           *testing.T
	krypton    *kr.Krypton
	xkr        *xe.XKr
	wait        chan *big.Int
	lastConfirm string
	wantNatSpec bool
}

func (self *testFrontend) UnlockAccount(acc []byte) bool {
	self.krypton.AccountManager().Unlock(common.BytesToAddress(acc), "password")
	return true
}

func (self *testFrontend) ConfirmTransaction(tx string) bool {
	if self.wantNatSpec {
		ds := docserver.New("/tmp/")
		self.lastConfirm = GetNotice(self.xkr, tx, ds)
	}
	return true
}

func testKr(t *testing.T) (krypton *kr.Krypton, err error) {

	os.RemoveAll("/tmp/kr-natspec/")

	err = os.MkdirAll("/tmp/kr-natspec/keystore", os.ModePerm)
	if err != nil {
		panic(err)
	}

	// create a testAddress
	ks := crypto.NewKeyStorePassphrase("/tmp/kr-natspec/keystore")
	am := accounts.NewManager(ks)
	testAccount, err := am.NewAccount("password")
	if err != nil {
		panic(err)
	}

	testAddress := strings.TrimPrefix(testAccount.Address.Hex(), "0x")

	db, _ := krdb.NewMemDatabase()
	// set up mock genesis with balance on the testAddress
	core.WriteGenesisBlockForTesting(db, common.HexToAddress(testAddress), common.String2Big(testBalance))

	// only use minimalistic stack with no networking
	krypton, err = kr.New(&kr.Config{
		DataDir:        "/tmp/kr-natspec",
		AccountManager: am,
		MaxPeers:       0,
		PowTest:        true,
		Krerbase:      common.HexToAddress(testAddress),
		NewDB:          func(path string) (common.Database, error) { return db, nil },
	})

	if err != nil {
		panic(err)
	}

	return
}

func testInit(t *testing.T) (self *testFrontend) {
	// initialise and start minimal krypton stack
	krypton, err := testKr(t)
	if err != nil {
		t.Errorf("error creating krypton: %v", err)
		return
	}
	err = krypton.Start()
	if err != nil {
		t.Errorf("error starting krypton: %v", err)
		return
	}

	// mock frontend
	self = &testFrontend{t: t, krypton: krypton}
	self.xkr = xe.New(krypton, self)
	self.wait = self.xkr.UpdateState()
	addr, _ := self.krypton.Krerbase()

	// initialise the registry contracts
	reg := registrar.New(self.xkr)
	var registrarTxhash, hashRegTxhash, urlHintTxhash string
	registrarTxhash, err = reg.SetGlobalRegistrar("", addr)
	if err != nil {
		t.Errorf("error creating GlobalRegistrar: %v", err)
	}

	hashRegTxhash, err = reg.SetHashReg("", addr)
	if err != nil {
		t.Errorf("error creating HashReg: %v", err)
	}
	urlHintTxhash, err = reg.SetUrlHint("", addr)
	if err != nil {
		t.Errorf("error creating UrlHint: %v", err)
	}
	if !processTxs(self, t, 3) {
		t.Errorf("error mining txs")
	}
	_ = registrarTxhash
	_ = hashRegTxhash
	_ = urlHintTxhash

	/* TODO:
	* lookup receipt and contract addresses by tx hash
	* name registration for HashReg and UrlHint addresses
	* mine those transactions
	* then set once more SetHashReg SetUrlHint
	 */

	return

}

// end to end test
func TestNatspecE2E(t *testing.T) {
	t.Skip()

	tf := testInit(t)
	defer tf.krypton.Stop()
	addr, _ := tf.krypton.Krerbase()

	// create a contractInfo file (mock cloud-deployed contract metadocs)
	// incidentally this is the info for the registry contract itself
	ioutil.WriteFile("/tmp/"+testFileName, []byte(testContractInfo), os.ModePerm)
	dochash := crypto.Sha3Hash([]byte(testContractInfo))

	// take the codehash for the contract we wanna test
	codeb := tf.xkr.CodeAtBytes(registrar.HashRegAddr)
	codehash := crypto.Sha3Hash(codeb)

	// use resolver to register codehash->dochash->url
	// test if globalregistry works
	// registrar.HashRefAddr = "0x0"
	// registrar.UrlHintAddr = "0x0"
	reg := registrar.New(tf.xkr)
	_, err := reg.SetHashToHash(addr, codehash, dochash)
	if err != nil {
		t.Errorf("error registering: %v", err)
	}
	_, err = reg.SetUrlToHash(addr, dochash, "file:///"+testFileName)
	if err != nil {
		t.Errorf("error registering: %v", err)
	}
	if !processTxs(tf, t, 5) {
		return
	}

	// NatSpec info for register method of HashReg contract installed
	// now using the same transactions to check confirm messages

	tf.wantNatSpec = true // this is set so now the backend uses natspec confirmation
	_, err = reg.SetHashToHash(addr, codehash, dochash)
	if err != nil {
		t.Errorf("error calling contract registry: %v", err)
	}

	fmt.Printf("GlobalRegistrar: %v, HashReg: %v, UrlHint: %v\n", registrar.GlobalRegistrarAddr, registrar.HashRegAddr, registrar.UrlHintAddr)
	if tf.lastConfirm != testExpNotice {
		t.Errorf("Wrong confirm message. expected\n'%v', got\n'%v'", testExpNotice, tf.lastConfirm)
	}

	// test unknown method
	exp := fmt.Sprintf(testExpNotice2, registrar.HashRegAddr)
	_, err = reg.SetOwner(addr)
	if err != nil {
		t.Errorf("error setting owner: %v", err)
	}

	if tf.lastConfirm != exp {
		t.Errorf("Wrong confirm message, expected\n'%v', got\n'%v'", exp, tf.lastConfirm)
	}

	// test unknown contract
	exp = fmt.Sprintf(testExpNotice3, registrar.UrlHintAddr)

	_, err = reg.SetUrlToHash(addr, dochash, "file:///test.content")
	if err != nil {
		t.Errorf("error registering: %v", err)
	}

	if tf.lastConfirm != exp {
		t.Errorf("Wrong confirm message, expected '%v', got '%v'", exp, tf.lastConfirm)
	}

}

func pendingTransactions(repl *testFrontend, t *testing.T) (txc int64, err error) {
	txs := repl.krypton.TxPool().GetTransactions()
	return int64(len(txs)), nil
}

func processTxs(repl *testFrontend, t *testing.T, expTxc int) bool {
	var txc int64
	var err error
	for i := 0; i < 50; i++ {
		txc, err = pendingTransactions(repl, t)
		if err != nil {
			t.Errorf("unexpected error checking pending transactions: %v", err)
			return false
		}
		if expTxc < int(txc) {
			t.Errorf("too many pending transactions: expected %v, got %v", expTxc, txc)
			return false
		} else if expTxc == int(txc) {
			break
		}
		time.Sleep(100 * time.Millisecond)
	}
	if int(txc) != expTxc {
		t.Errorf("incorrect number of pending transactions, expected %v, got %v", expTxc, txc)
		return false
	}

	err = repl.krypton.StartMining(runtime.NumCPU())
	if err != nil {
		t.Errorf("unexpected error mining: %v", err)
		return false
	}
	defer repl.krypton.StopMining()

	timer := time.NewTimer(100 * time.Second)
	height := new(big.Int).Add(repl.xkr.CurrentBlock().Number(), big.NewInt(1))
	repl.wait <- height
	select {
	case <-timer.C:
		// if times out make sure the xkr loop does not block
		go func() {
			select {
			case repl.wait <- nil:
			case <-repl.wait:
			}
		}()
	case <-repl.wait:
	}
	txc, err = pendingTransactions(repl, t)
	if err != nil {
		t.Errorf("unexpected error checking pending transactions: %v", err)
		return false
	}
	if txc != 0 {
		t.Errorf("%d trasactions were not mined", txc)
		return false
	}
	return true
}
