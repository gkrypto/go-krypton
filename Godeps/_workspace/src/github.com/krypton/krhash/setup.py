#!/usr/bin/env python
import os
from distutils.core import setup, Extension
sources = [
    'src/python/core.c',
    'src/libkrhash/io.c',
    'src/libkrhash/internal.c',
    'src/libkrhash/sha3.c']
if os.name == 'nt':
    sources += [
        'src/libkrhash/util_win32.c',
        'src/libkrhash/io_win32.c',
        'src/libkrhash/mmap_win32.c',
    ]
else:
    sources += [
        'src/libkrhash/io_posix.c'
    ]
depends = [
    'src/libkrhash/krhash.h',
    'src/libkrhash/compiler.h',
    'src/libkrhash/data_sizes.h',
    'src/libkrhash/endian.h',
    'src/libkrhash/krhash.h',
    'src/libkrhash/io.h',
    'src/libkrhash/fnv.h',
    'src/libkrhash/internal.h',
    'src/libkrhash/sha3.h',
    'src/libkrhash/util.h',
]
pykrhash = Extension('pykrhash',
                     sources=sources,
                     depends=depends,
                     extra_compile_args=["-Isrc/", "-std=gnu99", "-Wall"])

setup(
    name='pykrhash',
    author="Matthew Wampler-Doty",
    author_email="matthew.wampler.doty@gmail.com",
    license='GPL',
    version='0.1.23',
    url='https://github.com/krypton/krhash',
    download_url='https://github.com/krypton/krhash/tarball/v23',
    description=('Python wrappers for krhash, the krypton proof of work'
                 'hashing function'),
    ext_modules=[pykrhash],
)
