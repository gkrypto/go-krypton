/*
  This file is part of krhash.

  krhash is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  krhash is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with krhash.  If not, see <http://www.gnu.org/licenses/>.
*/

/** @file krhash.h
* @date 2015
*/
#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stddef.h>
#include "compiler.h"

#define KRASH_REVISION 23
#define KRASH_DATASET_BYTES_INIT 1073741824U // 2**30
#define KRASH_DATASET_BYTES_GROWTH 8388608U  // 2**23
#define KRASH_CACHE_BYTES_INIT 1073741824U // 2**24
#define KRASH_CACHE_BYTES_GROWTH 131072U  // 2**17
#define KRASH_EPOCH_LENGTH 30000U
#define KRASH_MIX_BYTES 128
#define KRASH_HASH_BYTES 64
#define KRASH_DATASET_PARENTS 256
#define KRASH_CACHE_ROUNDS 3
#define KRASH_ACCESSES 64
#define KRASH_DAG_MAGIC_NUM_SIZE 8
#define KRASH_DAG_MAGIC_NUM 0xFEE1DEADBADDCAFE

#ifdef __cplusplus
extern "C" {
#endif

/// Type of a seedhash/blockhash e.t.c.
typedef struct krhash_h256 { uint8_t b[32]; } krhash_h256_t;

// convenience macro to statically initialize an h256_t
// usage:
// krhash_h256_t a = krhash_h256_static_init(1, 2, 3, ... )
// have to provide all 32 values. If you don't provide all the rest
// will simply be unitialized (not guranteed to be 0)
#define krhash_h256_static_init(...)			\
	{ {__VA_ARGS__} }

struct krhash_light;
typedef struct krhash_light* krhash_light_t;
struct krhash_full;
typedef struct krhash_full* krhash_full_t;
typedef int(*krhash_callback_t)(unsigned);

typedef struct krhash_return_value {
	krhash_h256_t result;
	krhash_h256_t mix_hash;
	bool success;
} krhash_return_value_t;

/**
 * Allocate and initialize a new krhash_light handler
 *
 * @param block_number   The block number for which to create the handler
 * @return               Newly allocated krhash_light handler or NULL in case of
 *                       ERRNOMEM or invalid parameters used for @ref krhash_compute_cache_nodes()
 */
krhash_light_t krhash_light_new(uint64_t block_number);
/**
 * Frees a previously allocated krhash_light handler
 * @param light        The light handler to free
 */
void krhash_light_delete(krhash_light_t light);
/**
 * Calculate the light client data
 *
 * @param light          The light client handler
 * @param header_hash    The header hash to pack into the mix
 * @param nonce          The nonce to pack into the mix
 * @return               an object of krhash_return_value_t holding the return values
 */
krhash_return_value_t krhash_light_compute(
	krhash_light_t light,
	krhash_h256_t const header_hash,
	uint64_t nonce
);

/**
 * Allocate and initialize a new krhash_full handler
 *
 * @param light         The light handler containing the cache.
 * @param callback      A callback function with signature of @ref krhash_callback_t
 *                      It accepts an unsigned with which a progress of DAG calculation
 *                      can be displayed. If all goes well the callback should return 0.
 *                      If a non-zero value is returned then DAG generation will stop.
 *                      Be advised. A progress value of 100 means that DAG creation is
 *                      almost complete and that this function will soon return succesfully.
 *                      It does not mean that the function has already had a succesfull return.
 * @return              Newly allocated krhash_full handler or NULL in case of
 *                      ERRNOMEM or invalid parameters used for @ref krhash_compute_full_data()
 */
krhash_full_t krhash_full_new(krhash_light_t light, krhash_callback_t callback);

/**
 * Frees a previously allocated krhash_full handler
 * @param full    The light handler to free
 */
void krhash_full_delete(krhash_full_t full);
/**
 * Calculate the full client data
 *
 * @param full           The full client handler
 * @param header_hash    The header hash to pack into the mix
 * @param nonce          The nonce to pack into the mix
 * @return               An object of krhash_return_value to hold the return value
 */
krhash_return_value_t krhash_full_compute(
	krhash_full_t full,
	krhash_h256_t const header_hash,
	uint64_t nonce
);
/**
 * Get a pointer to the full DAG data
 */
void const* krhash_full_dag(krhash_full_t full);
/**
 * Get the size of the DAG data
 */
uint64_t krhash_full_dag_size(krhash_full_t full);

/**
 * Calculate the seedhash for a given block number
 */
krhash_h256_t krhash_get_seedhash(uint64_t block_number);

#ifdef __cplusplus
}
#endif
